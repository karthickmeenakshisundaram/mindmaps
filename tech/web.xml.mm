<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1475346691094" ID="ID_684402900" LINK="Jetty.mm" MODIFIED="1475346943821" STYLE="fork" TEXT="web.xml">
<node CREATED="1475346959613" ID="ID_284205584" MODIFIED="1475346966315" POSITION="right" TEXT="servlet-mapping">
<node CREATED="1475357657033" ID="ID_235478891" MODIFIED="1475357660327" TEXT="servlet-name"/>
<node CREATED="1475357660751" ID="ID_1135986209" MODIFIED="1475357672722" TEXT="url-pattern"/>
</node>
<node CREATED="1475346967025" ID="ID_817423232" MODIFIED="1475358075828" POSITION="right" TEXT="servlet">
<node CREATED="1475357642230" ID="ID_1235107756" MODIFIED="1475357645452" TEXT="servlet-name"/>
<node CREATED="1475357651487" ID="ID_1487828520" MODIFIED="1475357654391" TEXT="servlet-class"/>
<node CREATED="1475358055153" ID="ID_1102122178" MODIFIED="1475358059702" TEXT="init-param">
<node CREATED="1475358062523" ID="ID_1765385004" MODIFIED="1475358066052" TEXT="param-name"/>
<node CREATED="1475358066468" ID="ID_1516071626" MODIFIED="1475358068962" TEXT="param-value"/>
</node>
</node>
<node CREATED="1475357853465" ID="ID_1015078200" MODIFIED="1475358046215" POSITION="right" TEXT="filter">
<node CREATED="1475357857175" ID="ID_239266886" MODIFIED="1475357860375" TEXT="filter-name"/>
<node CREATED="1475357861022" ID="ID_547678120" MODIFIED="1475357864660" TEXT="filter-class"/>
<node CREATED="1475358055153" ID="ID_1641982805" MODIFIED="1475358059702" TEXT="init-param">
<node CREATED="1475358062523" ID="ID_1374932478" MODIFIED="1475358066052" TEXT="param-name"/>
<node CREATED="1475358066468" ID="ID_1464371619" MODIFIED="1475358068962" TEXT="param-value"/>
</node>
</node>
<node CREATED="1475357870834" ID="ID_1643062282" MODIFIED="1475357873817" POSITION="right" TEXT="filter-mapping">
<node CREATED="1475357874933" ID="ID_587759269" MODIFIED="1475357877363" TEXT="filter-name"/>
<node CREATED="1475357877818" ID="ID_1637454110" MODIFIED="1475357881195" TEXT="url-pattern"/>
</node>
<node CREATED="1475358097796" ID="ID_1822324850" MODIFIED="1475358102410" POSITION="left" TEXT="taglib">
<node CREATED="1475358105916" ID="ID_853526711" MODIFIED="1475358111037" TEXT="taglib-uri"/>
<node CREATED="1475358114516" ID="ID_443922128" MODIFIED="1475358118643" TEXT="taglib-location"/>
</node>
</node>
</map>
