<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1475996566767" ID="ID_1656153121" MODIFIED="1475996597320" TEXT="Android development Structure">
<node CREATED="1475996598707" ID="ID_1922583246" MODIFIED="1475996619636" POSITION="right" TEXT="android manifests">
<node CREATED="1475996663786" ID="ID_1374701446" MODIFIED="1475996686166" TEXT="Defines activities in the APK"/>
<node CREATED="1475996687155" ID="ID_1286174031" MODIFIED="1475996702835" TEXT="Also defines which are main ones and which are &apos;launchable&apos;"/>
</node>
<node CREATED="1475996621245" ID="ID_1071313865" MODIFIED="1475996637047" POSITION="right" TEXT="Activity Declaration in a class">
<node CREATED="1475996705391" ID="ID_1963472158" MODIFIED="1475996742006" TEXT=" extends AppCompatActivity"/>
<node CREATED="1475996745760" ID="ID_583829916" MODIFIED="1475996749560" TEXT="callback methods">
<node CREATED="1475996750844" ID="ID_1685264815" MODIFIED="1475996755698" TEXT="onCreate">
<node CREATED="1475996802394" ID="ID_397471576" MODIFIED="1475996804175" TEXT="onCreate(Bundle savedInstanceState)"/>
</node>
<node CREATED="1475996756224" ID="ID_648246630" MODIFIED="1475996760567" TEXT="onDestroy"/>
<node CREATED="1475996761078" ID="ID_1091327486" MODIFIED="1475996765570" TEXT="onStart"/>
<node CREATED="1475996766111" ID="ID_1876202053" MODIFIED="1475996771913" TEXT="onStop"/>
<node CREATED="1475996772655" ID="ID_1952825915" MODIFIED="1475996780657" TEXT="onResume"/>
<node CREATED="1475996781096" ID="ID_1593103528" MODIFIED="1475996785468" TEXT="onPause"/>
</node>
<node CREATED="1476005057895" ID="ID_281740510" MODIFIED="1476005150260">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="android_lifecycle" />
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1475996637964" ID="ID_1081634718" MODIFIED="1475996653293" POSITION="right" TEXT="presentation templates"/>
<node CREATED="1475996653741" ID="ID_485276273" MODIFIED="1475996657346" POSITION="right" TEXT="resources"/>
</node>
</map>
