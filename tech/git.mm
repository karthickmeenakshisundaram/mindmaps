<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1470935880717" ID="ID_1839754532" MODIFIED="1470942371833" TEXT="git">
<node CREATED="1470935939407" ID="ID_1576635508" MODIFIED="1470936201084" POSITION="right" TEXT="log">
<node CREATED="1470935948620" ID="ID_1571253806" MODIFIED="1470935951244" TEXT="-v">
<node CREATED="1470935952672" ID="ID_1733796605" MODIFIED="1470935955172" TEXT="Verbose"/>
</node>
<node CREATED="1470935956493" ID="ID_642642438" MODIFIED="1470935964278" TEXT="-p">
<node CREATED="1470935965396" ID="ID_760372932" MODIFIED="1470935971970" TEXT="Along with patch diff information"/>
</node>
<node CREATED="1470935973991" ID="ID_864768758" MODIFIED="1470935975534" TEXT="-2">
<node CREATED="1470935976946" ID="ID_70715100" MODIFIED="1470935982611" TEXT="Only 2 commits"/>
</node>
<node CREATED="1470935984409" ID="ID_848643143" MODIFIED="1470935990562" TEXT="--stat">
<node CREATED="1470935991863" ID="ID_1131859014" MODIFIED="1470936003729" TEXT="How many lines were changed per file and a summary"/>
</node>
<node CREATED="1470936005010" ID="ID_1447076422" MODIFIED="1470936022869" TEXT="--pretty">
<node CREATED="1470936023843" ID="ID_1001716653" MODIFIED="1470936030358" TEXT="=oneline"/>
<node CREATED="1470936031824" ID="ID_1634529060" MODIFIED="1470936036727" TEXT="=short"/>
<node CREATED="1470936037898" ID="ID_1477475756" MODIFIED="1470936040433" TEXT="=shorter"/>
<node CREATED="1470936041111" ID="ID_1477016818" MODIFIED="1470938672984" TEXT="=long"/>
<node CREATED="1470936048729" ID="ID_373453789" MODIFIED="1470936064252" TEXT="format:&apos;%s %am&apos;"/>
</node>
<node CREATED="1470936067727" ID="ID_1469407750" MODIFIED="1470936076867" TEXT="--graph"/>
<node CREATED="1470936098881" ID="ID_1463084886" MODIFIED="1470936101814" TEXT="--author">
<node CREATED="1470936110429" ID="ID_720868630" MODIFIED="1470936121497" TEXT="Restrict entries to author"/>
</node>
<node CREATED="1470936103161" ID="ID_1309968179" MODIFIED="1470936106246" TEXT="--committer">
<node CREATED="1470936124150" ID="ID_1052700284" MODIFIED="1470936130545" TEXT="Restrict Entries to Committer"/>
</node>
<node CREATED="1470936132296" ID="ID_1516885332" MODIFIED="1470938529214" TEXT="--grep">
<icon BUILTIN="help"/>
<node CREATED="1470936140158" ID="ID_846562536" MODIFIED="1470936149805" TEXT="grep for something in the commit message"/>
</node>
<node CREATED="1470936153956" ID="ID_430075416" MODIFIED="1470938544914" TEXT="-S">
<icon BUILTIN="help"/>
<node CREATED="1470936156767" ID="ID_821835773" MODIFIED="1470936176079" TEXT="bring log entries, where the diff contained the string"/>
</node>
<node CREATED="1470938409022" ID="ID_1898248088" MODIFIED="1470938477701">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      --since=4.weeks
    </p>
    <p>
      --after=4.weeks
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1470938421617" ID="ID_261472073" MODIFIED="1470938434134" TEXT="--before=4.years"/>
<node CREATED="1470938486967" ID="ID_132114840" MODIFIED="1470938490787" TEXT="--all-match">
<node CREATED="1470938492022" ID="ID_713763770" MODIFIED="1470938501815" TEXT="If grep and author are used together"/>
</node>
</node>
<node CREATED="1470936203635" ID="ID_915098027" MODIFIED="1470936271046" POSITION="left" TEXT="diff">
<node CREATED="1470936272832" ID="ID_1379165851" MODIFIED="1470936338099" TEXT="difference between staged and modified(aka uncommitted)"/>
<node CREATED="1470936360660" ID="ID_1903951245" MODIFIED="1470936388726">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      diff --cached
    </p>
    <p>
      diff --staged
    </p>
  </body>
</html></richcontent>
<node CREATED="1470936390714" ID="ID_1751322961" MODIFIED="1470938539830" TEXT="difference between staged and repository"/>
</node>
</node>
<node CREATED="1470936424950" ID="ID_1374698478" MODIFIED="1470936447384" POSITION="right" TEXT="commit">
<node CREATED="1470936453694" ID="ID_664530301" MODIFIED="1470936453694" TEXT="">
<node CREATED="1470936455680" ID="ID_879115892" MODIFIED="1470936480201" TEXT="brings up an editor with files changed for you to enter a commit message"/>
</node>
<node CREATED="1470936482378" ID="ID_1531766986" MODIFIED="1470936486344" TEXT="-v">
<node CREATED="1470936487454" ID="ID_1767700579" MODIFIED="1470936536405" TEXT="same as normal but makes the diff available for you to enter a commit message"/>
</node>
<node CREATED="1470936553050" ID="ID_159245244" MODIFIED="1470936555539" TEXT="--amend">
<node CREATED="1470936557914" ID="ID_166119746" MODIFIED="1470936568113" TEXT="amend a previous commit and let&apos;s you commit"/>
</node>
<node CREATED="1470938701338" ID="ID_410149004" MODIFIED="1470938703460" TEXT="-m ">
<node CREATED="1470938705707" ID="ID_1172340253" MODIFIED="1470938719856" TEXT="Commits directly with message in quotes"/>
</node>
<node CREATED="1470938726662" ID="ID_1516159299" MODIFIED="1470938731920" TEXT="-a -m">
<node CREATED="1470938733810" ID="ID_1945859887" MODIFIED="1470938756194" TEXT="skip the staging area and commit all files matched"/>
</node>
</node>
<node CREATED="1470936583669" ID="ID_224916692" MODIFIED="1470936590817" POSITION="left" TEXT="reset">
<node CREATED="1470936591807" ID="ID_451249617" MODIFIED="1470936605054" TEXT="HEAD">
<node CREATED="1470936635523" ID="ID_142424161" MODIFIED="1470936639841" TEXT="&lt;file&gt;">
<node CREATED="1470936653068" ID="ID_1746898870" MODIFIED="1470938113688" TEXT="unstage (aka reset the index for) file"/>
</node>
<node CREATED="1470936641936" ID="ID_1710377621" MODIFIED="1470936647985" TEXT="-- &lt;path&gt;">
<node CREATED="1470936663588" ID="ID_1148644451" MODIFIED="1470938157562" TEXT="unstage an entire path"/>
</node>
<node CREATED="1470936686975" ID="ID_769789421" MODIFIED="1470938205961" TEXT="--hard &lt;file&gt;">
<node CREATED="1470936699274" ID="ID_307174412" MODIFIED="1470938191593" TEXT="unstage file and replace file with one from repo"/>
</node>
</node>
</node>
<node CREATED="1470938315783" ID="ID_1406363701" MODIFIED="1470938597881" POSITION="left" TEXT="checkout">
<node CREATED="1470938322938" ID="ID_1159152072" MODIFIED="1470938328008" TEXT="&lt;file&gt;">
<node CREATED="1470938352499" ID="ID_1466569108" MODIFIED="1470938368175" TEXT="replace working copy file with file from repo"/>
</node>
<node CREATED="1470938331453" ID="ID_211575400" MODIFIED="1470938337449" TEXT="-- &lt;path&gt;">
<node CREATED="1470938376668" ID="ID_1169891063" MODIFIED="1470938385062" TEXT="replace working copy path with one from repo"/>
</node>
<node CREATED="1470942508636" ID="ID_693194681" MODIFIED="1470942532973" TEXT="-b &lt;branchname&gt; &lt;tagname&gt;">
<node CREATED="1470942534695" ID="ID_357508996" MODIFIED="1470942559971" TEXT="Create a new branch from tag and switch working copy to that"/>
</node>
</node>
<node CREATED="1470938845237" ID="ID_1843919269" MODIFIED="1470938849829" POSITION="right" TEXT="remote">
<node CREATED="1470938850938" ID="ID_1517275173" MODIFIED="1470938850938" TEXT="">
<node CREATED="1470938855901" ID="ID_634994379" MODIFIED="1470938860866" TEXT="Shows all remotes"/>
</node>
<node CREATED="1470938862114" ID="ID_1824827714" MODIFIED="1470938872385" TEXT="show &lt;remotename&gt;">
<node CREATED="1470938873717" ID="ID_91258696" MODIFIED="1470938879050" TEXT="shows a particular remote"/>
</node>
<node CREATED="1470938887487" ID="ID_1282789328" MODIFIED="1470938920099" TEXT="add &lt;remotename&gt; &lt;http or git or ssh url&gt;"/>
<node CREATED="1470938887487" ID="ID_421808161" MODIFIED="1470938937530" TEXT="rm &lt;remotename&gt; &lt;http or git or ssh url&gt;"/>
</node>
<node CREATED="1470938981795" ID="ID_1731881706" MODIFIED="1470938996309" POSITION="right" TEXT="fetch &lt;remotename&gt;">
<node CREATED="1470938997402" ID="ID_56895325" MODIFIED="1470939025421" TEXT="All branches from &lt;remotename are fetched&gt; and set up as &lt;remotename&gt;/&lt;branchname&gt;"/>
</node>
<node CREATED="1470940847318" ID="ID_1889900079" MODIFIED="1470940855007" POSITION="right" TEXT="pull &lt;remotename&gt;">
<node CREATED="1470940856794" ID="ID_1799422622" MODIFIED="1470940882829" TEXT="equivalent of a fetch+merge"/>
</node>
<node CREATED="1470940951479" ID="ID_308172497" MODIFIED="1470940966775" POSITION="left" TEXT="rm">
<node CREATED="1470940968091" ID="ID_338719255" MODIFIED="1470940971708" TEXT="&lt;file&gt;">
<node CREATED="1470940972992" ID="ID_1486288275" MODIFIED="1470940990976" TEXT="remove file from staging and working copy"/>
</node>
<node CREATED="1470941000169" ID="ID_1191231725" MODIFIED="1470941031612">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      --cached &lt;file&gt;
    </p>
    <p>
      --staged &lt;file&gt;
    </p>
  </body>
</html></richcontent>
<node CREATED="1470941012220" ID="ID_1112726635" MODIFIED="1470941021299" TEXT="remove file from staging"/>
</node>
</node>
<node CREATED="1470941057858" ID="ID_111592862" MODIFIED="1470941766629" POSITION="left" TEXT="tag">
<node CREATED="1470942904535" ID="ID_1150424587" MODIFIED="1470942914654" TEXT="">
<node CREATED="1470942907716" ID="ID_164814006" MODIFIED="1470942919891" TEXT="lists all tags"/>
</node>
<node CREATED="1470941067993" ID="ID_439672736" MODIFIED="1470941133687" TEXT="&lt;tagname&gt;">
<node CREATED="1470941069725" ID="ID_1237696442" MODIFIED="1470941151361" TEXT="create lightweight tag"/>
</node>
<node CREATED="1470941077531" ID="ID_1789153363" MODIFIED="1470941125843" TEXT="-a -m &lt;message&gt; &lt;tagname&gt;">
<node CREATED="1470941136121" ID="ID_405304268" MODIFIED="1470941147157" TEXT="create annotated tag"/>
</node>
<node CREATED="1470941814745" ID="ID_957309139" MODIFIED="1470941828143" TEXT="&lt;tagname&gt; &lt;hash&gt;">
<node CREATED="1470941828997" ID="ID_1269506433" MODIFIED="1470941849154" TEXT="Create tag with commit identified by hash"/>
</node>
</node>
<node CREATED="1470941785935" ID="ID_1737147129" MODIFIED="1470942302254" POSITION="left" TEXT="show &lt;tagname&gt;">
<node CREATED="1470942315626" ID="ID_177169939" MODIFIED="1470942328475" TEXT="Shows details regarding the tag"/>
</node>
<node CREATED="1470941895715" ID="ID_960375108" MODIFIED="1470941911751" POSITION="left" TEXT="config">
<node CREATED="1470941918110" ID="ID_1367014864" MODIFIED="1470941928018" TEXT="--global">
<node CREATED="1470941930107" ID="ID_1984106627" MODIFIED="1470941977314" TEXT="alias.&lt;aliasname&gt; &apos;&lt;commandname&gt;&apos;">
<node CREATED="1470942003232" ID="ID_1825966499" MODIFIED="1470942123810" TEXT="Eg1:  git config --global alias.ci &apos;commit -v&apos;"/>
<node CREATED="1470942056741" ID="ID_1260082738" MODIFIED="1470942118619" TEXT="Eg2: git config --global alias.co &apos;checkout&apos;"/>
<node CREATED="1470942125836" ID="ID_1196445474" MODIFIED="1470942140101" TEXT="Eg3. alias.stat &apos;status&apos;"/>
<node CREATED="1470942143698" ID="ID_1506162101" MODIFIED="1470942155689" TEXT="Eg4. alias.last &apos;log -1&apos;"/>
<node CREATED="1470942159317" ID="ID_79985849" MODIFIED="1470942177110" TEXT="Eg5. alias.br &apos;branch&apos;"/>
<node CREATED="1470942178862" ID="ID_658290686" MODIFIED="1470942221719" TEXT="Eg6. alias.unstage &apos;reset HEAD&apos;"/>
</node>
</node>
</node>
<node CREATED="1470942372996" ID="ID_1011005199" MODIFIED="1470942383422" POSITION="right" TEXT="push">
<node CREATED="1470942389340" ID="ID_1849526256" MODIFIED="1470942402292" TEXT="&lt;remotename&gt; &lt;branch/tag&gt;"/>
<node CREATED="1470942416394" ID="ID_1190200263" MODIFIED="1470942440825" TEXT="&lt;remotename&gt; --tags">
<node CREATED="1470942445806" ID="ID_1807004021" MODIFIED="1470942492589" TEXT="If you need to push all tags, without calling push on each tag"/>
</node>
</node>
</node>
</map>
