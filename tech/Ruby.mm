<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1476120833526" ID="ID_836990638" MODIFIED="1476136509441" TEXT="Ruby">
<hook NAME="accessories/plugins/RevisionPlugin.properties"/>
<node CREATED="1476120849995" FOLDED="true" ID="ID_1955557706" MODIFIED="1476136522442" POSITION="right" TEXT="keywords">
<node CREATED="1476120875061" ID="ID_1337017876" MODIFIED="1476120876274" TEXT="BEGIN&#x9;do&#x9;next&#x9;then END&#x9;else&#x9;nil&#x9;true alias&#x9;elsif&#x9;not&#x9;undef and&#x9;end&#x9;or&#x9;unless begin&#x9;ensure&#x9;redo&#x9;until break&#x9;false&#x9;rescue&#x9;when case&#x9;for&#x9;retry&#x9;while class&#x9;if&#x9;return&#x9;yield def&#x9;in&#x9;self&#x9;__FILE__ defined?&#x9;module&#x9;super&#x9;__LINE__"/>
</node>
<node CREATED="1476120899178" FOLDED="true" ID="ID_1527722623" MODIFIED="1476136509436" POSITION="right" TEXT="quirks">
<node CREATED="1476120934386" FOLDED="true" ID="ID_1806858967" MODIFIED="1476136509433" TEXT="whitespace">
<node CREATED="1476120938911" ID="ID_1154471975" MODIFIED="1476120958017" TEXT="a + b is interpreted as a+b ( Here a is a local variable)"/>
<node CREATED="1476120959920" ID="ID_1960810110" MODIFIED="1476120968329" TEXT="a  +b is interpreted as a(+b) ( Here a is a method call)"/>
</node>
<node CREATED="1476120970230" FOLDED="true" ID="ID_605471328" MODIFIED="1476136509433" TEXT="line endings">
<node CREATED="1476120982312" ID="ID_777401745" MODIFIED="1476121026528" TEXT="lines ending with +, - and \ mean line continues in next line"/>
</node>
<node CREATED="1476121039490" FOLDED="true" ID="ID_369556061" MODIFIED="1476136509434" TEXT="identifier aka names of variables/methods/constants">
<node CREATED="1476121057404" ID="ID_733871076" MODIFIED="1476121071604" TEXT="alnum"/>
<node CREATED="1476121072560" ID="ID_645896681" MODIFIED="1476121073790" TEXT="_"/>
</node>
<node CREATED="1476121109939" FOLDED="true" ID="ID_1724365163" MODIFIED="1476136509435" TEXT="Here Document">
<node CREATED="1476121114734" FOLDED="true" ID="ID_1477406609" MODIFIED="1476136509434">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      print &lt;&lt;ABC
    </p>
    <p>
      echo Hello world!!
    </p>
    <p>
      echo Hello Ruby Program!!!
    </p>
    <p>
      ABC
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1476121223457" FOLDED="true" ID="ID_1988240862" MODIFIED="1476136509434" TEXT="output">
<node CREATED="1476121193535" ID="ID_1410299458" MODIFIED="1476121218597">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      echo Hello world!!
    </p>
    <p>
      echo Hello Ruby Program!!!
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1476121114734" FOLDED="true" ID="ID_489512282" MODIFIED="1476136509434">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      print &lt;&lt;&quot;ABC&quot;
    </p>
    <p>
      echo Hello world!!
    </p>
    <p>
      echo Hello Ruby Program!!!
    </p>
    <p>
      &quot;ABC&quot;
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1476121223457" FOLDED="true" ID="ID_703092469" MODIFIED="1476136509434" TEXT="output-same">
<node CREATED="1476121193535" ID="ID_804670443" MODIFIED="1476121218597">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      echo Hello world!!
    </p>
    <p>
      echo Hello Ruby Program!!!
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1476121114734" FOLDED="true" ID="ID_406333603" MODIFIED="1476136509435">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      print &lt;&lt;`ABC`
    </p>
    <p>
      echo Hello world!!
    </p>
    <p>
      echo Hello Ruby Program!!!
    </p>
    <p>
      `ABC`
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1476121223457" FOLDED="true" ID="ID_1516532170" MODIFIED="1476136509435" TEXT="output- is actually an evaluation and print">
<node CREATED="1476121193535" ID="ID_535727819" MODIFIED="1476121364099">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Hello world!!
    </p>
    <p>
      Hello Ruby Program!!!
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1476121552260" FOLDED="true" ID="ID_1256300698" MODIFIED="1476136509435">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      print &lt;&lt;&quot;foo&quot;, &lt;&lt;&quot;bar&quot;&#160;&#160;# you can stack them&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
    </p>
    <p>
      I said foo.
    </p>
    <p>
      foo&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
    </p>
    <p>
      I said bar.
    </p>
    <p>
      bar
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1476121575698" ID="ID_955864702" MODIFIED="1476121587660">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      I said foo
    </p>
    <p>
      I said bar
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1476121613668" FOLDED="true" ID="ID_807592356" MODIFIED="1476136509436" TEXT="Initialization and destruction">
<node CREATED="1476121626411" ID="ID_63428474" MODIFIED="1476121654036">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      BEGIN{
    </p>
    <p>
      print &quot;Beginning stuff&quot;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1476121656172" ID="ID_800881077" MODIFIED="1476121697076">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      END{
    </p>
    <p>
      &#160;print &quot;Ending stuff&quot;
    </p>
    <p>
      }
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1476121703194" FOLDED="true" ID="ID_394754765" MODIFIED="1476136509436">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      print &quot;Doing stuff&quot;
    </p>
    <p>
      END{
    </p>
    <p>
      &#160;print &quot;Ending stuff&quot;
    </p>
    <p>
      }
    </p>
    <p>
      
    </p>
    <p>
      BEGIN{
    </p>
    <p>
      print &quot;Beginning stuff&quot;
    </p>
    <p>
      }
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1476121721012" ID="ID_1435599817" MODIFIED="1476121738805">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Beginning stuff
    </p>
    <p>
      Doing stuff
    </p>
    <p>
      Ending stuff
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1476121759889" FOLDED="true" ID="ID_1749041097" MODIFIED="1476136509436" TEXT="comments">
<node CREATED="1476121763712" ID="ID_1994002861" MODIFIED="1476121770940" TEXT="#Hello there"/>
<node CREATED="1476121771641" ID="ID_1658497088" MODIFIED="1476121792136">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      #Hello there
    </p>
    <p>
      #welcome to a lot of people
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1476121814426" ID="ID_345338366" MODIFIED="1476121864462">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      =begin
    </p>
    <p>
      Hello there. note the smaller case in the first line and the last line
    </p>
    <p>
      welcome to a lot of people
    </p>
    <p>
      =end
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1476121929477" ID="ID_1512095664" MODIFIED="1476136511151" POSITION="left" TEXT="Object oriented">
<node CREATED="1476122314012" FOLDED="true" ID="ID_1661740575" MODIFIED="1476136509436" TEXT="quirks">
<node CREATED="1476122594107" ID="ID_801335978" MODIFIED="1476122649383" TEXT="methods and characteristics are called data">
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1476122616085" ID="ID_771462001" MODIFIED="1476122634893" TEXT=""/>
</node>
<node CREATED="1476122578984" FOLDED="true" ID="ID_242406189" MODIFIED="1476136544254" TEXT="aspects">
<node CREATED="1476121968792" ID="ID_1554405495" MODIFIED="1476121971985" TEXT="Inheritance"/>
<node CREATED="1476121965139" ID="ID_1390766184" MODIFIED="1476121968480" TEXT="Polymorphism"/>
<node CREATED="1476121947639" ID="ID_1721947405" MODIFIED="1476121962680" TEXT="Data abstraction"/>
<node CREATED="1476121937831" ID="ID_1750770434" MODIFIED="1476121947208" TEXT="Data encapsulation"/>
</node>
<node CREATED="1476122665568" ID="ID_753161883" MODIFIED="1476136630275" TEXT="class">
<node CREATED="1476122790946" ID="ID_675114876" MODIFIED="1476136631880" TEXT="variables types">
<node CREATED="1476122806177" ID="ID_584220014" MODIFIED="1476136634795" TEXT="local">
<node CREATED="1476122853829" ID="ID_1760594615" MODIFIED="1476122861831" TEXT="start with lowercase letter or _"/>
<node CREATED="1476123028116" ID="ID_138300734" MODIFIED="1476123036222" TEXT="scope- only within method"/>
<node CREATED="1476136643390" ID="ID_856344488" MODIFIED="1476136698368" TEXT="scope ranges from class, module, def, &apos;do&apos; to &apos;end&apos;, { to }">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1476122808937" ID="ID_156183747" MODIFIED="1476136633487" TEXT="instance">
<node CREATED="1476122973883" ID="ID_294447819" MODIFIED="1476122977987" TEXT="start with @"/>
<node CREATED="1476123043421" ID="ID_856738068" MODIFIED="1476123079003" TEXT="scope- across methods but within the same object"/>
<node CREATED="1476136161788" ID="ID_282580615" MODIFIED="1476136166360" TEXT="default nil"/>
<node CREATED="1476136170417" FOLDED="true" ID="ID_1983179912" MODIFIED="1476136509437" TEXT="example">
<node BACKGROUND_COLOR="#cccccc" CREATED="1476136177232" ID="ID_1972806767" MODIFIED="1476136219445">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      #!/usr/bin/ruby
    </p>
    <p>
      
    </p>
    <p>
      class Customer
    </p>
    <p>
      &#160;&#160;&#160;def initialize(id, name, addr)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_id=id
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_name=name
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_addr=addr
    </p>
    <p>
      &#160;&#160;&#160;end
    </p>
    <p>
      &#160;&#160;&#160;def display_details()
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;puts &quot;Customer id #@cust_id&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;puts &quot;Customer name #@cust_name&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;puts &quot;Customer address #@cust_addr&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;end
    </p>
    <p>
      end
    </p>
    <p>
      
    </p>
    <p>
      # Create Objects
    </p>
    <p>
      cust1=Customer.new(&quot;1&quot;, &quot;John&quot;, &quot;Wisdom Apartments, Ludhiya&quot;)
    </p>
    <p>
      cust2=Customer.new(&quot;2&quot;, &quot;Poul&quot;, &quot;New Empire road, Khandala&quot;)
    </p>
    <p>
      
    </p>
    <p>
      # Call Methods
    </p>
    <p>
      cust1.display_details()
    </p>
    <p>
      cust2.display_details()
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1476122811424" FOLDED="true" ID="ID_635958061" MODIFIED="1476136509438" TEXT="class">
<node CREATED="1476123000682" ID="ID_1136134227" MODIFIED="1476123003647" TEXT="start with @@"/>
<node CREATED="1476123127610" ID="ID_704791121" MODIFIED="1476136413923" TEXT="scope- across different objects of the same class or within a module"/>
<node CREATED="1476123190529" ID="ID_1911098981" MODIFIED="1476123216744" TEXT="not across classes though"/>
<node CREATED="1476136420000" ID="ID_1710662313" MODIFIED="1476136426357" TEXT="must be initialized"/>
<node CREATED="1476136427345" FOLDED="true" ID="ID_280987391" MODIFIED="1476136509438" TEXT="example">
<node BACKGROUND_COLOR="#cccccc" CREATED="1476136436525" ID="ID_18977048" MODIFIED="1476136449723">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      #!/usr/bin/ruby
    </p>
    <p>
      
    </p>
    <p>
      class Customer
    </p>
    <p>
      &#160;&#160;&#160;@@no_of_customers=0
    </p>
    <p>
      &#160;&#160;&#160;def initialize(id, name, addr)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_id=id
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_name=name
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_addr=addr
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@@no_of_customers += 1
    </p>
    <p>
      &#160;&#160;&#160;end
    </p>
    <p>
      &#160;&#160;&#160;def display_details()
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;puts &quot;Customer id #@cust_id&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;puts &quot;Customer name #@cust_name&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;puts &quot;Customer address #@cust_addr&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;end
    </p>
    <p>
      &#160;&#160;&#160;&#160;def total_no_of_customers()
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;puts &quot;Total number of customers: #@@no_of_customers&quot;
    </p>
    <p>
      &#160;&#160;&#160;&#160;end
    </p>
    <p>
      end
    </p>
    <p>
      
    </p>
    <p>
      # Create Objects
    </p>
    <p>
      cust1=Customer.new(&quot;1&quot;, &quot;John&quot;, &quot;Wisdom Apartments, Ludhiya&quot;)
    </p>
    <p>
      cust2=Customer.new(&quot;2&quot;, &quot;Poul&quot;, &quot;New Empire road, Khandala&quot;)
    </p>
    <p>
      
    </p>
    <p>
      # Call Methods
    </p>
    <p>
      cust1.total_no_of_customers()
    </p>
    <p>
      cust2.total_no_of_customers()
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node CREATED="1476122813899" FOLDED="true" ID="ID_999254006" MODIFIED="1476136509439" TEXT="global">
<node CREATED="1476123171924" ID="ID_3698454" MODIFIED="1476123178491" TEXT="starts with $"/>
<node CREATED="1476123179550" ID="ID_1916186645" MODIFIED="1476123186297" TEXT="scope-across classes"/>
<node CREATED="1476135973542" ID="ID_1027682505" MODIFIED="1476135976746" TEXT="Avoid them"/>
<node CREATED="1476135980422" FOLDED="true" ID="ID_1992195278" MODIFIED="1476136509439" TEXT="example">
<node BACKGROUND_COLOR="#cccccc" CREATED="1476135984835" FOLDED="true" ID="ID_69601092" MODIFIED="1476136509438">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      #!/usr/bin/ruby
    </p>
    <p>
      
    </p>
    <p>
      $global_variable = 10
    </p>
    <p>
      class Class1
    </p>
    <p>
      &#160;&#160;def print_global
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;puts &quot;Global variable in Class1 is #$global_variable&quot;
    </p>
    <p>
      &#160;&#160;end
    </p>
    <p>
      end
    </p>
    <p>
      class Class2
    </p>
    <p>
      &#160;&#160;def print_global
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;puts &quot;Global variable in Class2 is #$global_variable&quot;
    </p>
    <p>
      &#160;&#160;end
    </p>
    <p>
      end
    </p>
    <p>
      
    </p>
    <p>
      class1obj = Class1.new
    </p>
    <p>
      class1obj.print_global
    </p>
    <p>
      class2obj = Class2.new
    </p>
    <p>
      class2obj.print_global
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1476135995383" FOLDED="true" ID="ID_292350478" MODIFIED="1476136509438" TEXT="output">
<node CREATED="1476136031406" ID="ID_1862903591" MODIFIED="1476136042090">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Global variable in Class1 is 10
    </p>
    <p>
      Global variable in Class1 is 10
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1476124328284" FOLDED="true" ID="ID_1979941309" MODIFIED="1476136509441" TEXT="example  definition">
<node CREATED="1476122670919" ID="ID_318902664" MODIFIED="1476122680957">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      class Car
    </p>
    <p>
      end
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1476124380802" ID="ID_207497547" MODIFIED="1476124411267">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      class Customer
    </p>
    <p>
      @@no_of_instances=0
    </p>
    <p>
      end
    </p>
  </body>
</html>
</richcontent>
</node>
<node BACKGROUND_COLOR="#cccccc" CREATED="1476124462407" FOLDED="true" ID="ID_197779304" MODIFIED="1476136509440">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      class Customer
    </p>
    <p>
      &#160;&#160;&#160;&#160;@@noofcustomers=0
    </p>
    <p>
      &#160;&#160;&#160;&#160;def initialize(id, name, addr)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_id=id
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_name=name
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_addr=addr
    </p>
    <p>
      &#160;&#160;&#160;&#160;end
    </p>
    <p>
      end
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1476133811634" ID="ID_1217703928" MODIFIED="1476133867286" TEXT="initialize is a special method- A constructor"/>
<node CREATED="1476134201784" FOLDED="true" ID="ID_1652233986" MODIFIED="1476136509440" TEXT="example method definition">
<node BACKGROUND_COLOR="#cccccc" CREATED="1476134218677" ID="ID_1050427623" MODIFIED="1476136290544">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      class Customer
    </p>
    <p>
      &#160;&#160;&#160;&#160;@@noofcustomers=0
    </p>
    <p>
      &#160;&#160;&#160;&#160;def initialize(id, name, addr)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_id=id
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_name=name
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;@cust_addr=addr
    </p>
    <p>
      &#160;&#160;&#160;&#160;end
    </p>
    <p>
      &#160;&#160;&#160;&#160;def printName()
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;puts @cust_name
    </p>
    <p>
      &#160;&#160;&#160;&#160;end
    </p>
    <p>
      end
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1476134302728" ID="ID_93839662" MODIFIED="1476134331059" TEXT="All method definition start with &apos;def&apos; and end with &apos;end&apos;"/>
</node>
</node>
</node>
<node CREATED="1476122707384" ID="ID_130198513" MODIFIED="1476122779741" TEXT="all variables, methods, constants are between class and end//so obvious isn&apos;t it?"/>
</node>
<node CREATED="1476124305880" FOLDED="true" ID="ID_263695549" MODIFIED="1476136540666" TEXT="instance definition">
<node BACKGROUND_COLOR="#ffff00" CREATED="1476134028130" ID="ID_1009247102" MODIFIED="1476135882383" TEXT="cust0=Customer.new"/>
<node BACKGROUND_COLOR="#ffff00" CREATED="1476134048069" ID="ID_1487897369" MODIFIED="1476135884330" TEXT="cust1=Customer.new(1,Karthick,&apos;100 Roseburn street&apos;)"/>
</node>
</node>
</node>
</map>
