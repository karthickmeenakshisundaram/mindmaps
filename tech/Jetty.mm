<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1475267604737" ID="ID_1470485694" MODIFIED="1475343813577" TEXT="Jetty">
<node CREATED="1475267643574" ID="ID_1458819840" MODIFIED="1475267645113" POSITION="right" TEXT="Modes">
<node CREATED="1475267612832" ID="ID_269086112" MODIFIED="1475267616690" TEXT="Standalone">
<node CREATED="1475345836399" ID="ID_679563842" MODIFIED="1475345983311" TEXT="port">
<node CREATED="1475345844288" ID="ID_559843796" MODIFIED="1475345879681" TEXT="start.ini/&lt;jetty.port&gt;"/>
</node>
<node CREATED="1475345984196" ID="ID_300207590" MODIFIED="1475345991791" TEXT="monitored directory">
<node CREATED="1475345993027" ID="ID_472932239" MODIFIED="1475346023970" TEXT="start.ini/&lt;jetty.deploy.monitoredDir&gt;"/>
</node>
<node CREATED="1475346429213" ID="ID_1160502726" MODIFIED="1475346434456" TEXT="activating modules">
<node CREATED="1475346438298" ID="ID_1964508856" MODIFIED="1475346444492" TEXT="start.ini">
<node CREATED="1475346445386" ID="ID_701346653" MODIFIED="1475346451921" TEXT="--module=logging"/>
</node>
<node CREATED="1475346454235" ID="ID_558243536" MODIFIED="1475346483314" TEXT="java -jar start.jar --add-to-startd=logging"/>
</node>
</node>
<node CREATED="1475267622472" ID="ID_252730921" MODIFIED="1475267631154" TEXT="Embedded">
<node CREATED="1475267655385" ID="ID_1399061352" MODIFIED="1475321758322" TEXT="1. Starting an embedded server with servlet turned on">
<node CREATED="1475321758314" ID="ID_1065284499" MODIFIED="1475321762075" TEXT="dependencies">
<node CREATED="1475321706463" ID="ID_1576543128" MODIFIED="1475321775671" TEXT="jetty-server"/>
<node CREATED="1475321776484" ID="ID_1960149776" MODIFIED="1475321778390" TEXT="jetty servlet"/>
</node>
<node CREATED="1475321785250" ID="ID_1706047326" MODIFIED="1475321873959" TEXT="program">
<node CREATED="1475321875092" ID="ID_1689128355" MODIFIED="1475321917413" TEXT="Server server = new Server(7070); &#xa;ServletContextHandler handler = new ServletContextHandler(server, &quot;/example&quot;); handler.addServlet(ExampleServlet.class, &quot;/&quot;); &#xa;server.start();"/>
<node CREATED="1475321930733" ID="ID_354620275" MODIFIED="1475322072455" TEXT="ExampleServlet is accessing at http://localhost:7070/example/&#xa;&#xa;Server opens up port 7070, &#xa;application context at /example and &#xa;servlet ExampleServlet.class mapped at /"/>
</node>
</node>
</node>
</node>
<node CREATED="1475343814632" ID="ID_831611985" MODIFIED="1475343821439" POSITION="left" TEXT="directories">
<node CREATED="1475343822428" ID="ID_325186187" MODIFIED="1475343835285" TEXT="etc">
<node CREATED="1475343984231" ID="ID_1117867826" MODIFIED="1475345378546" TEXT="configuration files for">
<node CREATED="1475345365141" ID="ID_557145787" MODIFIED="1475345367733" TEXT="jetty">
<node CREATED="1475345564692" ID="ID_1528727262" MODIFIED="1475345567456" TEXT="jetty.xml"/>
</node>
<node CREATED="1475345368163" ID="ID_1226297248" MODIFIED="1475345371616" TEXT="jetty modules">
<node CREATED="1475345568809" ID="ID_1292506933" MODIFIED="1475345652981" TEXT="jetty-logging.xml"/>
<node CREATED="1475345572613" ID="ID_84489538" MODIFIED="1475345572613" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1475343835661" ID="ID_291699086" MODIFIED="1475343837511" TEXT="modules">
<node CREATED="1475343895549" ID="ID_1085447824" MODIFIED="1475346146332" TEXT="all module are defined here as *.mod"/>
<node CREATED="1475346147232" ID="ID_1408093166" MODIFIED="1475346154486" TEXT="For example logging.mod">
<node CREATED="1475346155855" ID="ID_60089576" MODIFIED="1475346164892" TEXT="lists">
<node CREATED="1475346165969" ID="ID_1976666246" MODIFIED="1475346176006" TEXT="xml configuration file"/>
<node CREATED="1475346176437" ID="ID_1215206120" MODIFIED="1475346191199" TEXT="libs to be added to classpath"/>
<node CREATED="1475346191693" ID="ID_997844832" MODIFIED="1475346196564" TEXT="other files"/>
<node CREATED="1475346219104" ID="ID_849461083" MODIFIED="1475346223657" TEXT="ini template"/>
</node>
</node>
</node>
<node CREATED="1475345485381" ID="ID_1404617843" MODIFIED="1475345496581" TEXT="start.d">
<node CREATED="1475345509197" ID="ID_269257335" MODIFIED="1475345531775" TEXT="modules activated through command line are configured here"/>
</node>
<node CREATED="1475343838053" ID="ID_993709430" MODIFIED="1475343861282" TEXT="lib"/>
<node CREATED="1475343861657" ID="ID_1905469361" MODIFIED="1475343862799" TEXT="bin">
<node CREATED="1475343925764" ID="ID_204586714" MODIFIED="1475343944055" TEXT="contains jetty.sh - startup script"/>
</node>
<node CREATED="1475343863875" ID="ID_1272831559" MODIFIED="1475343865765" TEXT="webapps">
<node CREATED="1475344039195" ID="ID_521879397" MODIFIED="1475344082236" TEXT="This is the monitored directory for jetty."/>
<node CREATED="1475344083228" ID="ID_1164128049" MODIFIED="1475344084619" TEXT="Any webapps dropped within here will be hot deployed"/>
</node>
<node CREATED="1475343872496" ID="ID_455983629" MODIFIED="1475343879221" TEXT="resources">
<node CREATED="1475345429699" ID="ID_115121878" MODIFIED="1475345441407" TEXT="external configurations">
<node CREATED="1475345442428" ID="ID_767665817" MODIFIED="1475345457086" TEXT="log4j.properties"/>
</node>
</node>
</node>
<node CREATED="1475346656664" ID="ID_875898336" MODIFIED="1475346663746" POSITION="right" TEXT="deployment descriptor">
<node CREATED="1475346682674" ID="ID_1696359871" MODIFIED="1475346689345" TEXT="common for the entire container">
<node CREATED="1475346665093" ID="ID_1912804759" MODIFIED="1475346672201" TEXT="webDefault.xml"/>
</node>
<node CREATED="1475346694245" ID="ID_605251646" MODIFIED="1475346902588" STYLE="fork" TEXT="one for each webapp">
<node CREATED="1475346943861" ID="ID_881614639" LINK="web.xml.mm" MODIFIED="1475346943862" TEXT="web.xml"/>
</node>
</node>
</node>
</map>
