<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1467749935163" ID="ID_1055269224" MODIFIED="1467750034733" TEXT="ChessGame">
<node CREATED="1467749951875" ID="ID_1657285153" MODIFIED="1467749970381" POSITION="right" TEXT="ChessEngine">
<node CREATED="1467749971798" ID="ID_114999102" MODIFIED="1467749981377" TEXT="calculates the positions"/>
<node CREATED="1467749993107" ID="ID_234614049" MODIFIED="1467750027371" TEXT="based on the training bench and board"/>
<node CREATED="1467750123664" ID="ID_1680743892" MODIFIED="1467750133472" TEXT="can place pieces on the board"/>
</node>
<node CREATED="1467750035136" ID="ID_1422906821" MODIFIED="1467750037624" POSITION="left" TEXT="Board">
<node CREATED="1467750046459" ID="ID_172157695" MODIFIED="1467750053380" TEXT="has got n*m squares"/>
<node CREATED="1467750058779" ID="ID_1296713061" MODIFIED="1467750081376" TEXT="you can request squares from the board"/>
</node>
<node CREATED="1467750198704" ID="ID_701927716" LINK="Pieces.mm" MODIFIED="1467750198709" POSITION="right" TEXT="Pieces"/>
</node>
</map>
