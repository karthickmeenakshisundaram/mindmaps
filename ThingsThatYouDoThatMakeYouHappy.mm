<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1460142009946" ID="ID_914455997" MODIFIED="1460142960965" TEXT="ThingsThatMakeYouHappy">
<node CREATED="1460142029334" ID="ID_1318251724" MODIFIED="1460142975789" POSITION="right" TEXT="You trying &amp; failing &amp; knowing/getting to know the reasons for the failures and correcting and improving">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1460142095247" ID="ID_214228251" MODIFIED="1460142945615" POSITION="right" STYLE="fork" TEXT="When you see others are below your level">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1460142128257" ID="ID_1618658761" MODIFIED="1460142944630" TEXT="Thinking of yourself as guru and becoming lethargic and teaching others and they closing the gap">
<node CREATED="1460142353847" ID="ID_1689554955" MODIFIED="1460142944633" TEXT="You can teach them but you can&apos;t think you are guru and become lethargic"/>
<node CREATED="1460142533736" ID="ID_210050853" MODIFIED="1460142991481" TEXT="for that matter, what you&apos;ve learnt is just a handful of sand and what you&apos;ve not is as much as the earth, hence you are never guru and you should not teach anybody unless it is necessary and going to be helpful to you">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1460142236100" ID="ID_1758582214" MODIFIED="1460142944635" TEXT="Thinking that you are proceeding in the right direction and becoming resolute in your foundations">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</map>
